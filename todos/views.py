from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list_list": todo_list,
    }
    return render(request, "todos/todolist.html", context)


def todo_list_detail(request, id):
    todo_list_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todo_list_detail,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            list.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_list_update = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list_update)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm()
        context = {
            "form": form,
            "todo_list_object": todo_list_update,
        }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_list_delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list_delete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", task.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/itemcreate.html", context)


def todo_item_update(request, id):
    todo_item_update = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item_update)
        if form.is_valid():
            task = form.save()
            return redirect("todo_list_detail", id=task.id)
    else:
        form = TodoItemForm(instance=todo_item_update)
    context = {
        "form": form,
    }
    return render(request, "todos/itemupdate.html", context)
